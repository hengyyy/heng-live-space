package com.zhang.people.me;

import com.zhang.people.People;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MySelf implements People {
    String name;
    int age;


}
