package com.zhang.play.basketball;

import com.zhang.play.Play;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Basketball implements Play {
    @Bean("rest")
    public void rest() {
        System.out.println("休息一会儿!");
    }

    @Bean("playBasketball")
    public void play(){
        System.out.println("打篮球！");
    }

    @Bean("buyBasketball")
    public boolean buy(){
        System.out.println("买一个篮球!");
        return true;
    }

}
