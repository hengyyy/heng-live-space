package com.zhang.play.games;

import com.zhang.play.Play;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


//@Component
@Configuration
public class KOG implements Play {

    @Bean("playKOG")
    public void play(){

        System.out.println("玩一会儿王者荣耀！");
    }

    @Bean
    public void uninstall() {
        System.out.println("卸载王者荣耀！");
    }
}
